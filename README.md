# SuperBLT Autoupdate Example

## Configuring Your Mod for Autoupdates

You must have a place to host the zip file of your mod and the `meta.json` file.

Your `mod.txt` file must include the following:

```
  "updates": [
    {
      "identifier": "UniqueModNameHere",
      "host": {
          "meta": "https://example.com/meta.json"
        }
    }
  ]
```

Your `meta.json` file should follow this template:

```
[
  {
    "ident": "UniqueModNameHere",
    "version": "1.00",
    "download_url": "https://example.com/modfile.zip",
    "patchnotes_url": "https://example.com/patchnotes"
  }
]
```

## Updating Your Mod

1. Increment the version number in `mod.txt`
2. If you're using a `changelog.txt` file, be sure to add the changes that were made in the update
3. Zip the mod folder and upload it to your host
4. Update the changes in your `patchnotes` file that you specified in `meta.json`
5. If the mod zip file url changed, update the url in the `meta.json` file
6. Increment the `version` number in `meta.json`
7. Replace the `meta.json` file from your host with the new one. 

## Optional: Display Current Patchnotes in BLT Mods Menu

To get the current patchnotes (not the update patchnotes) to display in the `BLT Mods` section, you must include a `changelog.txt` file in the root of your mod folder. Here's [an example](https://i.imgur.com/RWwhAPe.png) of what it will look like. Note that this is not necessary to configure your mod to use autoupdates, but many modders might find this useful, and your users might find it helpful, so I included it as part of this tutorial.

## Troubleshooting

**Check your BLT log files (`mods > logs`) for any error messages.**

`[Downloads] Failed to rename old installation!`

 - This usually means you have an open file of the mod that's trying to update. Close it and try updating again

 `[Downloads] Downloaded mod is not a valid mod!`

 - Make sure the file structure of the mod zip file is correct. The zip file should contain a folder with the mod files inside of it

 `[Downloads] Failed to verify versions!`

 - Make sure that the version number in the update is higher than the version number in the current release

## Resources

1. [Automatic Updates](https://superblt.znix.xyz/doc/updates/)
1. [Hosting SuperBLT Mod Updates on GitHub](https://fragtrane.github.io/SuperBLT-Update-Demo/index.html)