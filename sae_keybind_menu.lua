local menu_title = 'SuperBLT Autoupdate Example'
local menu_message = [[Instructions:
1. Increment the version number in `mod.txt`
2. If you're using a `changelog.txt` file, be sure to add the changes that were made in the update
3. Zip the mod folder and upload it to your host
4. Update the changes in your `patchnotes` file that you specified in `meta.json`
5. If the mod zip file url changed, update the url in the `meta.json` file
6. Increment the `version` number in `meta.json`
7. Replace the meta.json file from your host with the new one.]]
local menu_options = {
	[1] = {
		text = 'Cancel',
		is_cancel_button = true,
	}
}

local menu = QuickMenu:new( menu_title, menu_message, menu_options )
menu:Show()